import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:mrbinjetores/telas/consulta_injetores.dart';

void main() {
  dotenv.load();
  runApp(MaterialApp(
    title: 'Flutter Demo',
    theme: ThemeData(
        primaryColor: Color(0xff004975),
        highlightColor: Color(0xff569FBF),
        appBarTheme: AppBarTheme(
          color: Color(0xff004975),
        )),
    initialRoute: '/',
    routes: {
      '/': (context) => ConsultaInjetoresPage(),
    },
  ));
}
