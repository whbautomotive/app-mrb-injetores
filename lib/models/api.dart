import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mrbinjetores/models/Injetor.dart';
import 'package:mrbinjetores/components/DialogAlert.dart';
import 'package:whbapi/whbapi.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class API extends WHBAPI {
  API() {
    print(dotenv.env);
    setAppShort(dotenv.env['APPSHORT']);
    setUrl(EnvType.dev, dotenv.env['DEV_URL']);
    setUrl(EnvType.prod, dotenv.env['PROD_URL']);
    setIdApp(dotenv.env['IDAPP']);
  }

  // Função para pega o token --------------------
  getToken() async {
    return '';
  }
}

final api = API();

Future<List> getAprovados(context, String cod) async {
  // api.setOption('debug', false);
  final resp = await api.sendGet('/consulta', {}, {'cod': cod});
  if (resp['code'] == 500) {
    await showDialogAlert(context, 'Opa!', resp['body']);
    return [];
  }
  if (resp['code'] == 404) {
    await showDialogAlert(context, 'Opa!', resp['body']);
    return [];
  }
  if (resp['code'] == 200) {
    var obj = jsonDecode(resp['body']);

    List<Injetor> newInjetores = List.generate(obj.length, (index) => null);
    obj.forEach((element) {
      var injet = Injetor.fromJson(element);
      newInjetores[obj.indexOf(element)] = injet;
    });
    return newInjetores;
  } else
    return List.empty();
}
