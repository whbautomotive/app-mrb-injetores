import 'package:flutter/material.dart';

class Injetor {
  int id;
  String data;
  String hora;
  String usuario;
  String injetor;
  String numero;
  int idproduto;
  double gap;
  double vazao100ml;
  double vazao100lib;
  double vazao50ml;
  double vazao50lib;
  double queda;
  String reqpolimento;
  String obs;
  int vezes;
  String aprovado;
  int identrega;
  String codigo;
  String datamatrix;
  int lote;
  double tammola;
  double assentoagulha;
  double assentocorpo;
  String cargamola;

  Injetor({
    this.id,
    this.data,
    this.hora,
    this.usuario,
    this.injetor,
    this.numero,
    this.idproduto,
    this.gap,
    this.vazao100ml,
    this.vazao100lib,
    this.vazao50ml,
    this.vazao50lib,
    this.queda,
    this.reqpolimento,
    this.obs,
    this.vezes,
    this.aprovado,
    this.identrega,
    this.codigo,
    this.datamatrix,
    this.lote,
    this.tammola,
    this.assentoagulha,
    this.assentocorpo,
    this.cargamola,
  });

  factory Injetor.fromJson(Map<String, dynamic> json) {
    return Injetor(
      id: json['id'],
      data: json['data'],
      hora: json['hora'],
      usuario: json['usuario'],
      injetor: json['injetor'],
      numero: json['numero'],
      idproduto: json['idproduto'],
      gap: json['gap'].toDouble(),
      vazao100ml: json['100ml'],
      vazao100lib: json['100lib'],
      vazao50ml: json['50ml'],
      vazao50lib: json['50lib'],
      queda: json['queda'].toDouble(),
      reqpolimento: json['reqpolimento'],
      obs: json['obs'],
      vezes: json['vezes'],
      aprovado: json['aprovado'],
      identrega: json['identrega'],
      codigo: json['codigo'],
      datamatrix: json['datamatrix'],
      lote: json['lote'],
      tammola: (json['tammola'] ?? 0).toDouble(),
      assentoagulha: (json['assentoagulha'] ?? 0).toDouble(),
      assentocorpo: (json['assentocorpo'] ?? 0).toDouble(),
      cargamola: json['cargamola'],
    );
  }

  Widget injetCard(context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      elevation: 4,
      margin: EdgeInsets.all(10.0),
      child: Padding(
        // padding: const EdgeInsets.fromLTRB(45, 20, 45, 20),
        padding: const EdgeInsets.all(10),
        child: Column(
          textBaseline: TextBaseline.alphabetic,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: Theme.of(context).primaryColor,
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(
                            text: TextSpan(
                              text: numero.toString(),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 26,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              text: datamatrix,
                              style: TextStyle(
                                color: Colors.white70,
                                fontSize: 12,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          RichText(
                            text: TextSpan(
                              text: data,
                              style: TextStyle(
                                color: Colors.white70,
                                fontSize: 16,
                              ),
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              text: hora,
                              style: TextStyle(
                                color: Colors.white70,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // dataPiece(label: 'ID', val: this.id.toString()),
            // dataPiece(label: 'aprovado', val: this.aprovado),
            // dataPiece(label: 'datamatrix', val: this.datamatrix),
            // dataPiece(label: 'data', val: this.data),
            // dataPiece(label: 'hora', val: this.hora),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                dataPiece(label: 'Injetor', val: this.injetor, pos: 2),
                dataPiece(
                    label: 'ID Produto',
                    val: this.idproduto.toString(),
                    pos: 1),
              ],
            ),
            // dataPiece(label: 'numero', val: this.numero),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                dataPiece(label: 'Gap', val: this.gap.toString(), pos: 2),
                dataPiece(label: 'Queda', val: this.queda.toString()),
                dataPiece(label: 'Vezes', val: this.vezes.toString(), pos: 1),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                dataPiece(
                    label: 'Vazao 100ml',
                    val: this.vazao100ml.toString(),
                    pos: 2),
                dataPiece(
                    label: 'Vazao 100lib',
                    val: this.vazao100lib.toString(),
                    pos: 1),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                dataPiece(
                    label: 'Vazao 50ml',
                    val: this.vazao50ml.toString(),
                    pos: 2),
                dataPiece(
                    label: 'Vazao 50lib',
                    val: this.vazao50lib.toString(),
                    pos: 1),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                dataPiece(
                    label: 'Tamanho Mola',
                    val: this.tammola.toString(),
                    pos: 2),
                dataPiece(label: 'Carga Mola', val: this.cargamola, pos: 1),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                dataPiece(
                    label: 'Assento Agulha',
                    val: this.assentoagulha.toString(),
                    pos: 2),
                dataPiece(
                    label: 'Assento Corpo',
                    val: this.assentocorpo.toString(),
                    pos: 1),
              ],
            ),
            // dataPiece(label: 'identrega', val: this.identrega.toString()),
            // dataPiece(label: 'codigo', val: this.codigo),
            // dataPiece(label: 'lote', val: this.lote.toString()),
            dataPiece(label: 'Requer Polimento', val: this.reqpolimento),
            // dataPiece(label: 'Observação', val: this.obs),
            Card(
              elevation: 0,
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: 'Obsevação',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Flexible(
                      child: RichText(
                        overflow: TextOverflow.visible,
                        maxLines: 4,
                        softWrap: true,
                        text: TextSpan(
                          text: obs,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Card dataPiece({String label, String val, int pos = 0}) {
    CrossAxisAlignment _calign;
    switch (pos) {
      case 2: // alinhado a direita
        _calign = CrossAxisAlignment.end;
        break;
      case 1: // alinhado a esquerda
        _calign = CrossAxisAlignment.start;
        break;
      default:
        _calign = CrossAxisAlignment.center;
        break;
    }

    return Card(
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: _calign,
          textBaseline: TextBaseline.alphabetic,
          children: [
            RichText(
              text: TextSpan(
                text: label,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            RichText(
              text: TextSpan(
                text: val,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
