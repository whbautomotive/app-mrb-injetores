import 'package:flutter/material.dart';
import 'package:mrbinjetores/models/Injetor.dart';
import 'package:mrbinjetores/models/api.dart';
import 'package:mrbinjetores/components/DialogAlert.dart';

class ConsultaInjetoresPage extends StatefulWidget {
  ConsultaInjetoresPage({Key key}) : super(key: key);

  @override
  _ConsultaInjetoresPageState createState() => _ConsultaInjetoresPageState();
}

class _ConsultaInjetoresPageState extends State<ConsultaInjetoresPage> {
  final _cod = new TextEditingController();
  var _msg = 'Digite o número';
  List _injetores = List.empty();

  void _busca(context, String cod) async {
    if (cod.isEmpty) {
      await showDialogAlert(
          context, 'nao informado', 'Por favor informe o numero/datamatrix');
      setState(() {
        _msg = 'Digite o número';
        _injetores = List.empty();
      });
      return;
    }
    var newInjetores = await getAprovados(context, cod);
    setState(() {
      _injetores = newInjetores;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Consulta Injetores'),
      ),
      body: Column(
        children: [
          Expanded(
            child: (_injetores.length >= 1)
                ? ListView.builder(
                    padding: EdgeInsets.fromLTRB(15, 10, 15, 0.0),
                    itemCount: _injetores.length,
                    itemBuilder: (context, i) {
                      return _injetores[i].injetCard(context);
                    })
                : Center(
                    child: SizedBox(
                      height: 300,
                      width: 300,
                      child: Card(
                        child: Center(
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              style: TextStyle(
                                  fontSize: 32, color: Colors.black54),
                              text: _msg,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 25),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  blurRadius: 3,
                  color: Color(0x77000000),
                )
              ],
              // color: Colors.red,
            ),
            alignment: Alignment.center,
            height: 85,
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _cod,
                    keyboardType: TextInputType.number,
                    onSubmitted: (txt) => _busca(context, txt),
                    decoration: InputDecoration(
                      hintText: 'numero / datamatrix',
                      border: InputBorder.none,
                    ),
                    // decoration: InputDecoration(
                    //   border: OutlineInputBorder(
                    //     borderRadius: BorderRadius.only(
                    //       topLeft: Radius.circular(5.0),
                    //       bottomLeft: Radius.circular(5.0),
                    //     ),
                    //   ),
                    //   filled: true,
                    //   fillColor: Colors.white,
                    //   labelText: 'numero / datamatrix',
                    // ),
                  ),
                ),
                SizedBox(width: 25),
                ClipOval(
                  child: Material(
                    color: Theme.of(context).primaryColor,
                    // borderRadius: BorderRadius.circular(25),
                    child: InkWell(
                      onTap: () => _busca(context, _cod.text),
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            stops: [0.0, 0.7],
                            colors: <Color>[
                              Color(0x00004975),
                              Theme.of(context).highlightColor,
                            ],
                          ),
                        ),
                        height: 50.0,
                        width: 50.0,
                        padding: EdgeInsets.all(10.0),
                        child: Icon(
                          Icons.search_rounded,
                          color: Colors.white,
                          size: 30.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
